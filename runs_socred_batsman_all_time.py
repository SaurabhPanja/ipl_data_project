import csv
from matplotlib import pyplot as plt
import list_replace_space_with_newline as space_to_newline


def get_runs_scored_by_batsmen():
    runs_scored_by_batsman = {}
    with open('deliveries.csv') as deliveries_csv:
        deliveries_csv_reader = csv.DictReader(deliveries_csv)
        for delivery in deliveries_csv_reader:
            batsman = delivery['batsman']
            batsman_runs = int(delivery['batsman_runs'])
            # print(batsman, batsman_runs)
            if batsman in runs_scored_by_batsman:
                runs_scored_by_batsman[batsman] += batsman_runs
            else:
                runs_scored_by_batsman[batsman] = batsman_runs
    return runs_scored_by_batsman


def get_top_n_batsmem(runs_scored_by_batsman, n=5):
    top_runs_list = []
    for batsman_runs in runs_scored_by_batsman.values():
        top_runs_list.append(batsman_runs)
    top_runs_list.sort(reverse=True)
    top_n_runs_list = top_runs_list[:n]
    top_n_batsmen_list = {}

    for top_n_runs in top_n_runs_list:
        for batsman, batsman_runs in runs_scored_by_batsman.items():
            if batsman_runs == top_n_runs:
                top_n_batsmen_list[batsman] = batsman_runs
    return top_n_batsmen_list


print(get_top_n_batsmem(get_runs_scored_by_batsmen()))


def plot_top_n_batsmen(runs_scored_by_batsman):
    batsman_x, batsman_runs_y = [], []
    for batsman, batsman_runs in runs_scored_by_batsman.items():
        batsman_x.append(batsman)
        batsman_runs_y.append(batsman_runs)
    plt.title("Top batsmen of all time", fontdict=None, loc='center', pad=None)
    plt.xlabel("Batsman", fontdict=None, labelpad=None)
    plt.ylabel("Runs", fontdict=None, labelpad=None)
    batsman_x_newline = space_to_newline.replace_space_with_newline(batsman_x)
    plt.bar(batsman_x_newline, batsman_runs_y)
    plt.show()
    return None


plot_top_n_batsmen(get_top_n_batsmem(get_runs_scored_by_batsmen()))
