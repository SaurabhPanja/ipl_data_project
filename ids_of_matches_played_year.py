import csv


def get_ids_of_matches_played(year):
    year = str(year)
    ids_of_matches_played = []
    with open("matches.csv") as match_csv_file:
        match_csv_file_reader = csv.DictReader(match_csv_file)
        for match in match_csv_file_reader:
            if match['season'] == year:
                ids_of_matches_played.append(match['id'])
    return ids_of_matches_played
