import csv
from matplotlib import pyplot as plt
import ids_of_matches_played_year
import list_replace_space_with_newline as space_to_newline


def get_extra_runs_by_team_2016(ids_of_matches_played_2016):
    extra_runs_by_team_2016 = {}
    with open("deliveries.csv") as deliveries_csv_file:
        deliveries_csv_file_reader = csv.DictReader(deliveries_csv_file)
        for delivery in deliveries_csv_file_reader:
            if delivery['match_id'] in ids_of_matches_played_2016:
                team = delivery['bowling_team']
                extra_runs = delivery['extra_runs']
                if delivery['is_super_over'] == '1':
                    continue
                if team in extra_runs_by_team_2016:
                    extra_runs_by_team_2016[team] += int(extra_runs)
                else:
                    extra_runs_by_team_2016[team] = int(extra_runs)
    return extra_runs_by_team_2016


def plot_extra_runs_team_2016(extra_runs_by_team_2016):
    teams, extra_runs = [], []

    for key, value in extra_runs_by_team_2016.items():
        teams.append(key)
        extra_runs.append(value)

    plt.title("Extra runs given per team",
              fontdict=None, loc='center', pad=None)
    plt.xlabel("Teams", fontdict=None, labelpad=None)
    plt.ylabel("Extra runs", fontdict=None, labelpad=None)
    teams_word_in_newline = space_to_newline.replace_space_with_newline(teams)
    plt.bar(teams_word_in_newline, extra_runs, width=0.5)
    plt.show()
    return None


plot_extra_runs_team_2016(get_extra_runs_by_team_2016(
    ids_of_matches_played_year.get_ids_of_matches_played(2016)))
