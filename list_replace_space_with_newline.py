def replace_space_with_newline(list_space):
    list_newline = []
    for word in list_space:
        list_newline.append(word.replace(' ', '\n'))
    return list_newline
