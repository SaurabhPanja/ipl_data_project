import csv
import matplotlib.pyplot as plt


def get_matches_won_by_all_teams():
    matches_won_by_all_teams = {}
    with open('matches.csv') as match_csv_file:
        match_csv_file_reader = csv.DictReader(match_csv_file)
        for match in match_csv_file_reader:
            team = match['winner']
            year = match['season']
            if team in matches_won_by_all_teams:
                if year in matches_won_by_all_teams[team]:
                    matches_won_by_all_teams[team][year] += 1
                else:
                    matches_won_by_all_teams[team][year] = 1
            else:
                matches_won_by_all_teams[team] = {year: 1}
    print(matches_won_by_all_teams)
    return matches_won_by_all_teams


def plot_matches_won_by_all_teams(matches_won_by_all_teams):
    all_years = []

    for year in range(2008, 2018):
        all_years.append(str(year))

    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', '#469990', '#808000',
              '#e6beff', '#9A6324', '#bfef45', '#fabebe', '#a9a9a9',  '#f58231']
    bottom_list = [0]*10
    team_list = []
    count = 0
    for team in matches_won_by_all_teams:
        year_win = []
        current_team = team
        if team == '':
            current_team = 'no_result'
        team_list.append(current_team)
        for year in all_years:
            if year in matches_won_by_all_teams[team]:
                year_win.append(matches_won_by_all_teams[team][year])
            else:
                year_win.append(0)

        plt.bar(all_years, year_win, 0.35, bottom_list, color=colors[count])
        count += 1
        for i in range(0, 10):
            bottom_list[i] += year_win[i]

    plt.ylabel('Number of matches')
    plt.title('Number of Matches won by teams in each season')
    plt.xlabel("Year")
    plt.yticks(list(range(0, 200, 10)))
    plt.legend(team_list, ncol=2)
    plt.show()
    return 0


plot_matches_won_by_all_teams(get_matches_won_by_all_teams())
