import csv
from matplotlib import pyplot as plt


def get_matches_played_everyyear():
    matches_played_everyyear = {}
    with open('matches.csv') as match_csv_file:
        match_csv_file_reader = csv.DictReader(match_csv_file)
        for match in match_csv_file_reader:
            year = match['season']
            if year in matches_played_everyyear:
                matches_played_everyyear[year] += 1
            else:
                matches_played_everyyear[year] = 1
    return matches_played_everyyear


def plot_matches_played_everyyear(matches_played_everyyear):
    years, matches = [], []
    for keys, values in matches_played_everyyear.items():
        years.append(keys)
        matches.append(values)

    plt.title("Number of matches played everyyear",
              fontdict=None, loc='center', pad=None)
    plt.xlabel("Year", fontdict=None, labelpad=None)
    plt.ylabel("Number of matches played", fontdict=None, labelpad=None)
    plt.bar(years, matches)
    plt.show()
    return None


plot_matches_played_everyyear(get_matches_played_everyyear())

# calculate
# plot
# execute
