# Ipl Data Project

#### Dependencies

[Matplotlib](https://matplotlib.org/)

The application is build using **PYTHON** programming language and **MATPLOTLIB** library.

From the given CSV files that have details of every match and deliveries bowled in **IPL** from the year 2008 to 2017, few problem sets were given from which by applying proper logic and using right data structure the graphs are plotted using **MATPLOTLIB** library.

## Problem Set

    In this data assignment you will transform raw data from IPL into graphs that will convey some meaning / analysis. For each part of this assignment you will have 2 parts -

    Code python functions that will transform the raw csv data into a data structure in a format suitable for plotting with matplotlib.

    Generate the following plots ...

    1. Plot the number of matches played per year of all the years in IPL.
    2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.
    3. For the year 2016 plot the extra runs conceded per team.
    4. For the year 2015 plot the top economical bowlers.

## Solution

##### Problem 1

![problem1](https://i.ibb.co/Mgd5TxV/numbers-of-matches-played-year.png)

##### Problem 2

![problem2](https://i.ibb.co/PmRjC82/Matches-won-per-team.png)

##### Problem 3

![problem3](https://i.ibb.co/ZH7j1VV/extra-runs-conceded-by-teams-2016.png)

##### Problem 4

![problem4](https://i.ibb.co/9tmgX7X/top-economocial-bowler-2015.png)

#### Leading run Scorer in _IPL_ till _2017_

![Leading Run Scorer](https://i.ibb.co/1qBmgs7/leading-run-scorer.png)
