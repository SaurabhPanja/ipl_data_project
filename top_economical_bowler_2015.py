import csv
from matplotlib import pyplot as plt
import ids_of_matches_played_year
import list_replace_space_with_newline as space_to_newline


def get_ids_of_matches_played_2015(year):
    return ids_of_matches_played_year.get_ids_of_matches_played(year)


def get_bowler_run_concede_2015(ids_of_matches_played_2015):
    bowler_run_conceded_2015 = {}
    with open('deliveries.csv') as deliveries_csv:
        deliveries_csv_reader = csv.DictReader(deliveries_csv)
        for delivery in deliveries_csv_reader:
            if delivery['match_id'] in ids_of_matches_played_2015:
                bowler = delivery['bowler']
                wide_runs = int(delivery['wide_runs'])
                noball_runs = int(delivery['noball_runs'])
                batsmen_runs = int(delivery['batsman_runs'])
                total_runs_delivery = wide_runs + noball_runs + batsmen_runs
                if delivery['is_super_over'] == '1':
                    continue
                if bowler in bowler_run_conceded_2015:
                    if 'runs' in bowler_run_conceded_2015[bowler] and 'balls' in bowler_run_conceded_2015[bowler]:
                        bowler_run_conceded_2015[bowler]['runs'] += total_runs_delivery
                        bowler_run_conceded_2015[bowler]['balls'] += 1
                    else:
                        bowler_run_conceded_2015[bowler]['runs'] = total_runs_delivery
                        bowler_run_conceded_2015[bowler]['balls'] = 1
                    if wide_runs > 0 or noball_runs > 0:
                        bowler_run_conceded_2015[bowler]['balls'] -= 1
                else:
                    bowler_run_conceded_2015[bowler] = {
                        'runs': total_runs_delivery,
                        'balls': 1
                    }
                    if wide_runs > 0 or noball_runs > 0:
                        bowler_run_conceded_2015[bowler]['balls'] -= 1
    return bowler_run_conceded_2015


def get_economy_of_bowler_2015(bowler_run_conceded_2015):
    economy_of_bowler_2015 = {}
    for bowler in bowler_run_conceded_2015:
        economy = (bowler_run_conceded_2015[bowler]['runs']
                   * 6) / bowler_run_conceded_2015[bowler]['balls']
        economy_of_bowler_2015[bowler] = round(economy, 2)
    return economy_of_bowler_2015


def plot_economy_of_top_n_bowler_2015(economy_of_bowler_2015, n=5):
    bowler_name_x, bowler_economy_y = [], []
    for key, value in economy_of_bowler_2015.items():
        bowler_name_x.append(key)
        bowler_economy_y.append(value)

    bowler_economy_y.sort()

    top_n_bowler_economy = []
    top_n_bowler_name = []
    for economy in bowler_economy_y[:n]:
        for key, value in economy_of_bowler_2015.items():
            if value == economy:
                top_n_bowler_name.append(key)
                top_n_bowler_economy.append(value)
    plt.title("Top Economical bowler in 2015",
              fontdict=None, loc='center', pad=None)
    plt.xlabel("Bowler", fontdict=None, labelpad=None)
    plt.ylabel("Economy", fontdict=None, labelpad=None)
    top_n_bowler_name_newline = space_to_newline.replace_space_with_newline(
        top_n_bowler_name)
    plt.bar(top_n_bowler_name_newline, top_n_bowler_economy)
    plt.show()
    return None


# execute
plot_economy_of_top_n_bowler_2015(get_economy_of_bowler_2015(
    get_bowler_run_concede_2015(get_ids_of_matches_played_2015(2015))))
